# -*- coding: utf-8 -*-

from util.utils import validate_type
from model.Locacao import Locacao
from model.Cliente import Cliente
from model.Funcionario import FuncionarioComum, FAdministrador
from model.Cupom import Cupom
from model.Midia import Status as StatusMidia


class Locadora():
    def __init__(self):
        self._nome = ''
        self._clientes = set()
        self._funcionarios = set()
        self._midias = set()

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, _nome):
        self._nome = _nome

    def alugar_cd(self, _funcionario, _cliente, _nome_cd, _data_locacao, _data_devolucao):
        validate_type(_funcionario, FuncionarioComum)
        validate_type(_cliente, Cliente)
        locacao = Locacao(_data_locacao, _data_devolucao)
        cliente = self.busca_cliente(_cliente.cpf)
        cd = _nome_cd

        if cd is None:
            raise IndexError('Mídia Indisponível.')

        cupom = Cupom(cliente, locacao, cd)

        cupom.generate_cupom()

    def add_funcionario(self, _funcionario):
        self._funcionarios.add(_funcionario)

    def remove_funcionario(self, _funcionario):
        self._funcionarios.remove(_funcionario)

    # Implementar busca funcionario

    def add_midia(self, _midia):
        self._midias.add(_midia)

    def remove_midia(self, _midia):
        self._midias.remove(_midia)

    def add_cliente(self, _cliente):
        self._clientes.add(_cliente)

    def remove_cliente(self, _cliente):
        self._clientes.remove(_cliente)

    def busca_cliente(self, _cliente_nome):
        for _cliente in self._clientes:
            return _cliente if _cliente.nome is _cliente_nome else None

    def busca_midia_disponivel(self, _nome_midia):
        return self._busca_midia_status(_nome_midia, StatusMidia.DISPONIVEL)

    def busca_midia_indisponivel(self, _nome_midia):
        return self._busca_midia_status(_nome_midia, StatusMidia.ALUGADO)

    def busca_midia_titulo_status(self, _nome_midia, _status):
        _midia_encontrada = None
        for _midia in self._midias:
            if _midia.titulo is _nome_midia and _midia.status is _status:
                _midia_encontrada = _midia

        return _midia_encontrada

    def busca_cliente(self, _cpf):
        _cliente_encontrado = None
        for _cliente in self._clientes:
            if _cliente.cpf is _cpf:
                _cliente_encontrado = _cliente
        return _cliente_encontrado