# -*- coding: utf-8 -*-

from util.utils import validate_type
from abc import ABCMeta
from model.Midia import DVD, CD, GAME


class Funcionario(object, metaclass=ABCMeta):
    def __init__(self):
        self._nome = ''
        self._senha = ''
        self._endereco = ''
        self._telefone = 0
        self._cpf = 0

        raise BaseException('This class could not be instantiated because is abstract.')

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, _nome):
        validate_type(_nome, str)
        self._nome = _nome

    @property
    def senha(self):
        return self._senha

    @senha.setter
    def senha(self, _senha):
        validate_type(_senha, str)
        self._senha = _senha

    @property
    def endereco(self):
        return self._endereco

    @endereco.setter
    def endereco(self, _endereco):
        validate_type(_endereco, str)
        self._endereco = _endereco

    @property
    def telefone(self):
        return self._telefone

    @telefone.setter
    def telefone(self, _telefone):
        validate_type(_telefone, int)

    @property
    def cpf(self):
        return self._cpf

    @cpf.setter
    def cpf(self, _cpf):
        validate_type(_cpf, int)
        self._cpf = _cpf

    def create_dvd(self, _numero, _censura, _titulo, _categoria, _genero, _idioma):
        dvd = DVD()
        dvd.numero = _numero
        dvd.censura = _censura
        dvd.titulo = _titulo
        dvd.categoria = _categoria
        dvd.genero = _genero
        dvd.idioma = _idioma

        return dvd

    def create_cd(self,  _numero, _censura, _titulo, _categoria, _nome):
        cd = CD()
        cd.nome = _nome
        cd.numero = _numero
        cd.censura = _censura
        cd.titulo = _titulo
        cd.categoria = _categoria

        return cd

    def create_game(self, _numero, _censura, _titulo, _categoria, _console):
        game = GAME()
        game.nome = _console
        game.numero = _numero
        game.censura = _censura
        game.titulo = _titulo
        game.categoria = _categoria

        return game


class FuncionarioComum(Funcionario):

    def __init__(self,  _nome='', _cpf=0, _endereco='', _telefone=0):
        self._cpf = _cpf
        self._nome = _nome
        self._endereco = _endereco
        self._telefone = _telefone


class FAdministrador(Funcionario):
    def __init__(self):
        pass

    def create_funcionario_comum(self, _nome, _cpf, _endereco, _telefone):
        return FuncionarioComum(_nome, _cpf, _endereco, _telefone)