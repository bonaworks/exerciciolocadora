# -*- coding: utf-8 -*-

from util.utils import validate_type

from model.Cliente import Cliente
from model.Locacao import Locacao
from model.Midia import Midia


class Cupom:
    def __init__(self, _cliente, _locacao, _midia):
        validate_type(_cliente, Cliente)
        validate_type(_locacao, Locacao)


        self._cliente = _cliente
        self._locacao = _locacao
        self._midia = _midia

    @property
    def cliente(self):
        return self._cliente

    @property
    def locacao(self):
        return self._locacao

    @property
    def midia(self):
        return self._midia

    # Como esse método não possui setters os valores de cliente e locacao não podem ser
    # alterados após sua criação

    def generate_cupom(self):
        text = 'locacao feita'
        print(text)
