# -*- coding: utf-8 -*_

from abc import ABCMeta
from enum import Enum
from util.utils import validate_type


class Genero(Enum):
    ACTION = 'AÇÃO'
    DRAMA = 'DRAMA'
    TERROR = 'TERROR'


class Tipo(Enum):
    JOGO = 'JOGO'
    MUSICA = 'MÚSICA'
    FILME = 'FILME'

class Status(Enum):
    ALUGADO = 'ALUGADO'
    DISPONIVEL = 'DISPONIVEL'


class Categoria(Enum):
    LANCAMENTO = 'LANÇAMENTO'
    COMUM = 'COMUM'
    ACERVO = 'ACERVO'

    def equals(self, _categoria):
        validate_type(_categoria, Categoria)
        return self == _categoria


class Midia(object, metaclass=ABCMeta):

    def __init__(self):
        self._numero = 0
        self._censura = 0
        self._tipo = Tipo
        self._titulo = ''
        self._categoria = Categoria
        self._status = Status

        raise BaseException('Media could not be instantiated.')

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, _status):
        validate_type(_status, Status)
        self._status = _status

    @property
    def categoria(self):
        return self._categoria

    @categoria.setter
    def categoria(self, _categoria):
        validate_type(_categoria, Categoria)

        self._categoria = _categoria

    @property
    def numero(self):
        return self._numero

    @numero.setter
    def numero(self, _numero):
        validate_type(_numero, int)
        self._numero = _numero

    @property
    def censura(self):
        return self._censura

    @censura.setter
    def censura(self, _censura):
        validate_type(_censura, int)
        self._censura = _censura

    @property
    def tipo(self):
        return self._tipo

    @tipo.setter
    def tipo(self, _tipo):
        validate_type(_tipo, Tipo)
        self._tipo = _tipo

    @property
    def titulo(self):
        return self._titulo

    @titulo.setter
    def titulo(self, _titulo):
        validate_type(_titulo, str)
        self._titulo = _titulo


class DVD(Midia):

    def __init__(self):
        self._genero = Genero
        self._idioma = ''

    @property
    def genero(self):
        return self._genero

    @genero.setter
    def genero(self, _genero):
        validate_type(_genero, Genero)
        self._genero = _genero

    @property
    def idioma(self):
        return self._idioma

    @idioma.setter
    def idioma(self, _idioma):
        validate_type(_idioma, str)
        self._idioma = _idioma


class CD(Midia):
    def __init__(self):
        self.__nome = ''

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, _nome):
        validate_type(_nome, str)
        self._nome = _nome


class GAME(Midia):
    def __init__(self):
        self._nome_do_console = ''

    @property
    def nome_do_console(self):
        return self._nome_do_console

    @nome_do_console.setter
    def nome_do_console(self, _nome_do_console):
        validate_type(_nome_do_console, str)
        self._nome_do_console = _nome_do_console


