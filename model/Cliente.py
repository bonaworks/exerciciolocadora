#-*- coding: utf-8 -*-

from util.utils import validate_type


class Cliente:
    def __init__(self):
        self._nome = ''
        self._cpf = 0
        self._telefone = 0
        self._pessoas_autorizadas = set()

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, _nome):
        validate_type(_nome, str)
        self._nome = _nome

    @property
    def cpf(self):
        return self._cpf

    @cpf.setter
    def cpf(self, _cpf):
        validate_type(_cpf, int)
        self._cpf = _cpf

    @property
    def telefone(self):
        return self._telefone

    @telefone.setter
    def telefone(self, _telefone):
        validate_type(_telefone, int)
        self._telefone = _telefone


    def add_pessoa_autorizada(self, _pessoa):
        validate_type(_pessoa, Cliente)
        if len(self._pessoas_autorizadas) >= 5:
            raise IndexError('Could not more authorize anyone. Only 5 is allowed.')
        self._pessoas_autorizadas.add(_pessoa)

    def remove_pessoa_autorizada(self, _pessoa):
        validate_type(_pessoa, Cliente)
        self._pessoas_autorizadas.remove(_pessoa)

    def busca_pessoa_autorizada(self, _cpf):
        validate_type(_cpf, int)
        for _pessoa in self._pessoas_autorizadas:
            if _pessoa.cpf is _cpf:
                return _pessoa

        raise IndexError('Person with cpf = ' + _cpf + ' could not be found.')