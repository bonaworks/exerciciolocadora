# -*- coding: utf-8 -*-

from util.utils import validate_type
from datetime import date


class Locacao:
    def __init__(self, _data_locacao, _data_devolucao):
        validate_type(_data_locacao, date)
        validate_type(_data_devolucao, date)

        self._data_locacao = _data_locacao
        self._data_devolucao = _data_devolucao

    @property
    def data_locacao(self):
        return self._data_locacao

    @data_locacao.setter
    def data_locacao(self, _data_locacao):
        validate_type(_data_locacao, date)

    @property
    def data_devolucao(self):
        return self._data_devolucao

    @data_devolucao.setter
    def data_devolucao(self, _data_devolucao):
        validate_type(_data_devolucao, date)
        self._data_devolucao = _data_devolucao
