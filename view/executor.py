# -*- coding: utf-8 -*-

from model.Cliente import Cliente
from model.Cupom import Cupom
from model.Midia import DVD, CD, GAME, Genero, Categoria, Tipo, Status
from model.Locacao import Locacao
from model.Funcionario import FuncionarioComum, FAdministrador
from model.Locadora import Locadora
from datetime import date

if __name__ == '__main__':
    # Adicionar itens locadora

    cd1 = CD()
    cd1.status = Status.DISPONIVEL
    cd1.nome = 'Bah'
    cd1.censura = 1
    cd1.numero = 1232313
    cd1.categoria = Categoria.ACERVO
    cd1.titulo = 'Bah'
    cd1.tipo = Tipo.MUSICA

    # Um cliene vai à locadora e aluga uma midia
    locadora = Locadora()
    funcionario = FuncionarioComum()
    funcionario.nome = 'Joaquim Nabuco'
    funcionario.cpf = 8889990918
    funcionario.endereco = 'Rua das Alegrias Perenes, 887'
    funcionario.telefone = 98800099987
    cliente = Cliente()
    cliente.nome = 'Paulo Roberto'
    cliente.telefone = 8889987763
    cliente.cpf = 77764655399
    locadora.add_cliente(cliente)
    locadora.alugar_cd(funcionario, cliente, 'Bah', date.today(), date(2018, 7, 5))
