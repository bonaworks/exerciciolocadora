# -*- coding: utf-8 -*-

def validate_type(_object, _type):
    if type(_object) is _type:
        return True
    raise TypeError('Parameter should be from type ' + _type)
